//
//  ViewController.swift
//  tvPogoda
//
//  Created by WSR on 6/21/19.
//  Copyright © 2019 WSR. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MapKit // библиотека для подключения карты

class ViewController: UIViewController {
 
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var pogodaLabel: UILabel!
    @IBOutlet weak var veter: UILabel!
    @IBOutlet weak var temper: UILabel!
    @IBOutlet weak var City: UILabel!
    
    var city = "Moscow"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadData(city: city)
    }
    func downloadData(city: String) {
        //проверка на выборку значения города на втором экране
        if let cityStr: String = UserDefaults.standard.string(forKey: "City") {
            self.city = cityStr
        }
        //находим токен
        let token = "1e936ee21707e2a418e98dca00877357"
        //прописываем ссылку
        let urlStr = "https://api.openweathermap.org/data/2.5/weather?q=\(city)&units=metric&appid=\(token)"
      //  let url = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        //отправляем запрос к на сервер
        Alamofire.request(urlStr, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                //записываем значение в наш город
                self.City.text = self.city
                // в label temper записываем температуру (запрос с ервера)
                self.temper.text = json["main"]["temp"].stringValue+"°C"
                //аналогично записываем скорость вертра
                self.veter.text = json["wind"]["speed"].stringValue + " м/с"
                // пабота с pogodaLabel
                self.pogodaLabel.text = json["weather"][0]["description"].stringValue
                
            //работа с иконками
                let  icon = json["weather"][0]["icon"].stringValue
                let imageStr = "https://api.openweathermap.org/img/w/" + (icon) + ".png"
                let imageUrl = URL(string: imageStr)
                let data = try? Data(contentsOf: imageUrl!)
                self.icon.image = UIImage(data: data!)
                
                
                //если запрос не срабатывает то выдаем ошибку
            case .failure(let error):
                print(error)
            }
        }
        
    }

}


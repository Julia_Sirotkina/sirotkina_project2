//
//  MapViewController.swift
//  tvPogoda
//
//  Created by WSR on 6/22/19.
//  Copyright © 2019 WSR. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MapKit

//UITextFieldDelegate добавляет в класс дополнительные функции к которорым мы будем обращаться
class MapViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var temper: UILabel!
    @IBOutlet weak var City: UILabel!
    @IBOutlet weak var zapros: UITextField!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var poleCity: UIView!
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        zapros.delegate = self
    }
    func downloadData(city: String) {
    
        //находим тонек
        let token = "1e936ee21707e2a418e98dca00877357"
        //прописываем ссылку
        let urlStr = "https://api.openweathermap.org/data/2.5/weather?q=\(city)&units=metric&appid=\(token)"
        //  let url = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        //отправляем запрос к на сервер
        Alamofire.request(urlStr, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                //записываем значение в наш город
                self.City.text = city
                // в label temper записываем температуру (запрос с ервера)
                self.temper.text = json["main"]["temp"].stringValue + "°C"
                let lat = json["coord"]["lat"].doubleValue
                let lon = json["coord"]["lon"].doubleValue
                self.showCityOnMap(lat: lat, lon: lon)
                
                //работа с иконками
                let  icon = json["weather"][0]["icon"].stringValue
                let imageStr = "https://api.openweathermap.org/img/w/" + (icon) + ".png"
                let imageUrl = URL(string: imageStr)
                let data = try? Data(contentsOf: imageUrl!)
                self.icon.image = UIImage(data: data!)
                
                self.zapros.text = ""
            //если запрос не срабатывает то выдаем ошибку
            case .failure(let error):
                print(error)
                self.zapros.text = ""
                self.zapros.placeholder = "Город не найден!"
            }
        }
        
    }
    func showCityOnMap(lat: Double, lon: Double) {
        //
        let regionRadius: CLLocationDistance = 10000
        //
        let cordinateRegion = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: lat, longitude: lon), latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        //для карты передаем координаты региона и ставим анимацию (true плавное перемещение)
        mapView.setRegion(cordinateRegion, animated: true)
    }
    //
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        downloadData(city: zapros.text!)
        
        
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
